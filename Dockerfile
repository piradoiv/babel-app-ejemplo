FROM golang:1.14-alpine as build
ADD /src /src
RUN cd /src && CGO_ENABLED=0 go build -o /server

#---------------------------

FROM scratch
COPY --from=build /server /server
ENTRYPOINT ["/server"]
EXPOSE 8000
